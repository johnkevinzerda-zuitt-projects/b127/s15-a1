function gradeConverter(grade) {
	if (grade<=100&&grade>=80) {
		console.log('Your Grade is A')
	} else if (grade<=79&&grade>=75) {
		console.log('Your Grade is B+')
	} else if (grade<=74&&grade>=65) {
		console.log('Your Grade is B')
	} else if (grade<=64&&grade>=55) {
		console.log('Your Grade is C+')
	} else if (grade<=54&&grade>=50) {
		console.log('Your Grade is C')
	} else if (grade<=49&&grade>=40) {
		console.log('Your Grade is D')
	} else if(grade<40&&grade>=0) {
		console.log('Your Grade is F')
	} else {
		console.log('Please enter a number between 100 - 0')
	}
}
//test
gradeConverter(81);
gradeConverter(75);
gradeConverter(74);
gradeConverter(55);
gradeConverter(49);
gradeConverter(39);
gradeConverter(100.00001);
gradeConverter(-0.00001);